/*
 * ESP8266 side of the RPI-ESP8226-MQTT-Bridge
 *
 * Author: Dave Amphlett
 */
#include "painlessMesh.h"
#include "ArduinoJson.h"
#include "QueueList.h"

#define   MESH_SSID               "ithang"
#define   MESH_PASSWORD           "RJWJ?zEaq&Ag6@?b"
#define   MESH_PORT               5555
#define   MESH_CHANNEL            1

#define   SERIAL_IN_CHECK_MSECS   330
#define   SERIAL_IN_PROCESS_MSECS 550
#define   SERIAL_SEND_QUEUE_SIZE  10
#define   SERIAL_SEND_QUEUE_MSECS 290
#define   SERIAL_KEEP_ALIVE_SECS  30

// =============================================================================
// ====== GENERAL GLOBAL VARIABLES =============================================
// =============================================================================
Scheduler _userScheduler; // to control your personal task
painlessMesh  _mesh;

// =============================================================================
// ====== FUNCTION PROTOTYPES ==================================================
// =============================================================================

// ====== Serial Request Handlers ==============================================
void serialRequestHandle(String receivedLine);
void serialRequestHandlerComment(String comment);
void serialRequestHandlerKA(String kaId);
void serialRequestHandlerMSG(String jsonStr);
void serialRequestHandlerCTL(String jsonStr);
void serialRequestHandlerCTLStatus(JsonObject& root);

String buildStatusResponseStr();
void buildGetNodeListResponse(JsonObject& responseRoot);

// ====== Mesh Callbacks =======================================================
void meshCallbackReceived( const uint32_t &from, const String &msg );
void meshCallbackNewConnection(uint32_t nodeId);
void meshCallbackChangedConnection();
void meshCallbackNodeTimeAdjusted(int32_t offset);

// ====== Scheduled Task Callbacks =============================================
void taskCallbackSendHelloWorld();
void taskCallbackSerialReadIncomming();
void taskCallbackSerialProcessIncomming();
void taskCallbackSerialSendQueued();
void taskCallbackSerialSendKeepAlive();

// ====== High Level Serial Functions ==========================================
bool serialSendMsg(String topic, String body);
bool serialSendMsgJson(String source, String topic, JsonObject& rootmsg);
bool serialSendKeepAlive();
bool serialSendResponseJson(JsonObject& root);
bool serialSendResponseRaw(String msg);

// ====== Low Level Serial Sending =============================================
// bool serialSend(String str);
void sendSerialQueued();
bool serialSendLineEnd();
bool serialSendLine(String str);
bool serialSendJsonLine(String prefix, JsonObject& root);
bool serialSendComment(String msg);
bool serialSendCommentPrintf(const char* fmt, ...);

// ====== Low Level Serial Receiving ===========================================
String _serialInBuffer;
unsigned long _lastAliveMsgNum = 1;
bool serialReadLine(String& line, String lineEnd);

// ====== Low Level General Utility Functions ==================================
String urldecode(String str);
String urlencode(String str);
unsigned char h2int(char c);
void mergeJsonObjects(JsonObject& src, JsonObject& dest);
uint32_t stringToUint32t(String& val);
String jsonCombineToString(JsonObject& root, String key, JsonObject& rhsObj);
QueueList<String> serialInProcessQueue;
QueueList<String> serialSendQueue;


// ====== Schedulable Tasks ==================================
Task taskObjSendHelloWorldMessage( TASK_SECOND * 5 , TASK_FOREVER, &taskCallbackSendHelloWorld );
Task taskObjSerialReadIncomming( TASK_MILLISECOND * SERIAL_IN_CHECK_MSECS , TASK_FOREVER, &taskCallbackSerialReadIncomming );
Task taskObjSerialProcessIncomming( TASK_MILLISECOND * SERIAL_IN_PROCESS_MSECS , TASK_FOREVER, &taskCallbackSerialProcessIncomming );
Task taskObjSerialSendQueued( TASK_MILLISECOND * SERIAL_SEND_QUEUE_MSECS , TASK_FOREVER, &taskCallbackSerialSendQueued );
Task taskObjSerialSendKeepAlive( TASK_SECOND * SERIAL_KEEP_ALIVE_SECS , TASK_FOREVER, &taskCallbackSerialSendKeepAlive );

// =============================================================================
// ====== Setup ================================================================
// =============================================================================
void setup() {
    Serial.begin(115200);

    serialSendComment("Setup");

    // _mesh.setDebugMsgTypes( ERROR | MESH_STATUS | CONNECTION | SYNC | COMMUNICATION | GENERAL | MSG_TYPES | REMOTE ); // all types on
    _mesh.setDebugMsgTypes( ERROR | STARTUP );  // set before init() so that you can see startup messages

    _mesh.init( MESH_SSID, MESH_PASSWORD, &_userScheduler, MESH_PORT, WIFI_AP_STA, MESH_CHANNEL );
    _mesh.onReceive(&meshCallbackReceived);
    _mesh.onNewConnection(&meshCallbackNewConnection);
    _mesh.onChangedConnections(&meshCallbackChangedConnection);
    _mesh.onNodeTimeAdjusted(&meshCallbackNodeTimeAdjusted);

    _userScheduler.addTask( taskObjSerialReadIncomming );
    taskObjSerialReadIncomming.enable();

    _userScheduler.addTask( taskObjSerialProcessIncomming );
    taskObjSerialProcessIncomming.enable();

    _userScheduler.addTask( taskObjSerialSendQueued );
    taskObjSerialSendQueued.enable();

    _userScheduler.addTask( taskObjSerialSendKeepAlive );
    taskObjSerialSendKeepAlive.enable();

    // _userScheduler.addTask( taskObjSendHelloWorldMessage );
    // taskObjSendHelloWorldMessage.enable();
}

// =============================================================================
// ====== Load =================================================================
// =============================================================================
void loop() {
  _userScheduler.execute(); // it will run mesh scheduler as well
  _mesh.update();
}

// ==========================================================786573728===================
// ====== Serial Request Handlers ==============================================
// =============================================================================

void serialRequestHandle(String receivedLine) {
  if (receivedLine.startsWith("#")) {
      serialRequestHandlerComment(receivedLine.substring(1));

  } else if (receivedLine.startsWith("KA:")) {
      serialRequestHandlerKA(receivedLine.substring(strlen("KA:")));

  } else if (receivedLine.startsWith("MSG:")) {
      serialRequestHandlerMSG(receivedLine.substring(strlen("MSG:")));

  } else if (receivedLine.startsWith("CTL:")) {
      serialRequestHandlerCTL(receivedLine.substring(strlen("CTL:")));

  }
}

void serialRequestHandlerComment(String comment) {
  serialSendComment("Received a comment...");
  if (comment.startsWith("boo")) {
    serialSendComment("That's scary!");
  } else if (comment.startsWith("help")) {
    serialSendComment("not much I can serialSendQueuedo from here I'm afraid");
  }
}

void serialRequestHandlerKA(String kaId) {
  serialSendComment("received keep alive ["+kaId+"]");
}

void serialRequestHandlerMSG(String jsonStr) {
  serialSendComment("received message ["+jsonStr+"]");
  DynamicJsonBuffer jsonBuffer;
  JsonObject& root = jsonBuffer.parseObject(jsonStr);
  if ( ! (root.containsKey("src") && root.containsKey("tpc") && root.containsKey("msg")) ) {
    serialSendComment("Error: Can't process this: must have src, tpc and msg fields at the root level");
    String stuff; root.printTo(stuff);
    serialSendComment("Error: "+stuff);
    return;
  }

  String topic = root["tpc"];
  if (topic.startsWith("painlessMesh/to/gateway")) {
    // serialSendComment("  - message to the gateway itself");
    String msg = root["msg"];
    // serialSendComment("  - msg body ["+msg+"]");
    if (msg == "getStatus") {
      // serialSendComment("  - inside getStatus handling");
      String response = buildStatusResponseStr();
      // serialSendComment("sending:["+response+"]");
      serialSendMsg("gateway", "painlessMesh/from/gateway", response);
    }
    // serialSendComment("  - done handling getStatus");
  } else if (topic.startsWith("painlessMesh/to/")) {
    String toNodeIdStr = topic.substring(strlen("painlessMesh/to/"));
    uint32_t toNodeId = stringToUint32t(toNodeIdStr);
    String msg = root["msg"];
    // serialSendComment("  - sending on message to node A ["+toNodeIdStr+"]");
    // serialSendComment("  - sending on message to node B ["+String(toNodeId)+"]");
    _mesh.sendSingle(toNodeId, msg);
  } else if (topic.startsWith("painlessMesh/toAll")) {
    String msg = root["msg"];
    // serialSendComment("  - sending on message to all nodes");
    _mesh.sendBroadcast(msg, false);
  }

  // serialSendComment("  - leaving serialRequestHandlerMSG()");
}

void serialRequestHandlerCTL(String jsonStr) {
  serialSendComment("received control instruction");
  DynamicJsonBuffer jsonBuffer;
  JsonObject& root = jsonBuffer.parseObject(jsonStr);
  if ( ! root.containsKey("command")) {
    serialSendComment("Error: Can't process this: must specify a command at the root level");
    return;
  }

  String command = root["command"];
  serialSendComment("Control Instruction: Command="+command);
  if (command == "status") {
    serialRequestHandlerCTLStatus(root);
  } else if (command == "getNodeList") {
    serialRequestHandlerCTLGetNodeList(root);
  }
}

void serialRequestHandlerCTLStatus(JsonObject& commandRoot) {
  serialSendComment("Status control command received");
  serialSendResponseRaw(buildStatusResponseStr());
}

void serialRequestHandlerCTLGetNodeList(JsonObject& commandRoot) {
  serialSendComment("getNodeList control command received");
  serialSendComment("Nodes:"+_mesh.subConnectionJson());

  DynamicJsonBuffer jsonBuffer;
  JsonObject& responseRoot = jsonBuffer.createObject();

  buildGetNodeListResponse(responseRoot);
  serialSendResponseJson(responseRoot);
}

String buildStatusResponseStr() {
  String resp = "{\"command\": \"status\"";
  resp += String(",\"mesh_ip\":\"")   + IPAddress(_mesh.getAPIP()).toString() + String("\"");
  resp += String(",\"node_id\":")   + String(_mesh.getNodeId());
  resp += String(",\"node_time\":") + String(_mesh.getNodeTime());
  resp += String(",\"free_mem\":")  + String(ESP.getFreeHeap());
  resp += String(",\"stability\":") + String(_mesh.stability);
  resp += String(",\"nodes\":") + _mesh.subConnectionJson();
  resp += String("}");
  return resp;
}

void buildGetNodeListResponse(JsonObject& responseRoot) {
  responseRoot["command"] = "getNodeList";
  // DynamicJsonBuffer jsonBufferForNodes;
  // JsonArray& nodesArray = jsonBufferForNodes.parseArray(_mesh.subConnectionJson());
  // responseRoot.set("nodes", nodesArray);
  responseRoot["nodes"] = RawJson(_mesh.subConnectionJson());
}


// =============================================================================
// ====== Mesh Callbacks =======================================================
// =============================================================================
void meshCallbackReceived( const uint32_t &from, const String &msg ) {
  serialSendCommentPrintf("bridge: Received from mesh %u msg=%s", from, msg.c_str());
  String source = String(from);
  String topic = "painlessMesh/from/" + String(from);
  serialSendMsg(source, topic, msg);
}

void meshCallbackNewConnection(uint32_t nodeId) {
    serialSendCommentPrintf("New Connection, nodeId = %u", nodeId);
}

void meshCallbackChangedConnection() {
    serialSendCommentPrintf("Changed connections %s",_mesh.subConnectionJson().c_str());
}

void meshCallbackNodeTimeAdjusted(int32_t offset) {
    serialSendCommentPrintf("Adjusted time %u. Offset = %d", _mesh.getNodeTime(),offset);
}

// =============================================================================
// ====== Scheduled Task Callbacks =============================================
// =============================================================================
void taskCallbackSendHelloWorld() {
  String msg = "Hello from node ";
  msg += _mesh.getNodeId();
  _mesh.sendBroadcast( msg );
}

// =============================================================================
// ====== High Level Serial Functions ==========================================
// =============================================================================
bool serialSendMsg(String source, String topic, String body) {
  body = urlencode(body);
  DynamicJsonBuffer jsonBuffer;
  JsonObject& root = jsonBuffer.createObject();
  root["src"] = source;
  root["tpc"] = topic;
  root["msg"] = body;
  // serialSend("MSG:");
  return serialSendJsonLine("MSG:",root);
}

bool serialSendMsgJson(String source, String topic, JsonObject& rootMsg) {
  String msg;
  rootMsg.printTo(msg);
  return serialSendMsg(source, topic, msg);
}

bool serialSendKeepAlive() {
  return serialSendLine("KA:"+String(_lastAliveMsgNum++));
}

bool serialSendResponseJson(JsonObject& root) {
  //serialSend("RSP:");
  return serialSendJsonLine("RSP:", root);
}

bool serialSendResponseRaw(String msg) {
  //serialSend("RSP:");
  return serialSendLine(String("RSP")+msg);
}



// =============================================================================
// ====== Low Level Serial Sending =============================================
// =============================================================================
// bool serialSend(String str) {
//   return (bool)Serial.print(str.c_str());
// }

bool sendSerialQueueString(String str) {
  // Serial.print("Q:+ "); for(int i=0;i<serialSendQueue.count();i++){Serial.print("#");} Serial.println();

  // if (serialSendQueue.count() < SERIAL_SEND_QUEUE_SIZE) {
    serialSendQueue.push(str);
    return true;
  // } else {
    // return false;
  // }
}

void sendSerialQueued() {
  // Serial.print("Q:@ "); for(int i=0;i<serialSendQueue.count();i++){Serial.print("#");} Serial.println();
  while (serialSendQueue.count() > 0) {
    Serial.println(serialSendQueue.pop());
    // Serial.print("Q:- "); for(int i=0;i<serialSendQueue.count();i++){Serial.print("#");} Serial.println();
  }
}

bool serialSendLineEnd() {
  return sendSerialQueueString("");
  //return (bool)Serial.println();
}

bool serialSendLine(String str) {
  return sendSerialQueueString(str);
  // serialSend(str);
  // return (bool)serialSendLineEnd();
}

bool serialSendJsonLine(String prefix, JsonObject& root) {
  String jsonStr;
  root.printTo(jsonStr);
  return sendSerialQueueString(prefix + jsonStr);
  // return (bool)Serial.println();
}

bool serialSendComment(String msg) {
  return sendSerialQueueString("#"+msg);
  // serialSend("#");
  // return (bool)serialSendLine(msg);
}

bool serialSendCommentPrintf(const char* fmt, ...) {
  char buf[128]; // resulting string limited to 128 chars
  va_list args;
  va_start(args, fmt );
  vsnprintf(buf, 128, fmt, args);
  va_end(args);
  return sendSerialQueueString(String("#")+String(buf));
  // Serial.print("#");
  // return (bool)Serial.println(buf);
}

// =============================================================================
// ====== Low Level Serial Receiving ===========================================
// =============================================================================

void taskCallbackSerialReadIncomming() {
    if (serialReadLine(_serialInBuffer, "\r\n")) {
        String line = String(_serialInBuffer.c_str());
        serialInProcessQueue.push(line);
        _serialInBuffer = "";
    }
}

void taskCallbackSerialProcessIncomming() {
    while (serialInProcessQueue.count() > 0) {
        String line = serialInProcessQueue.pop();
        serialSendCommentPrintf("Processing Serial input line:[%s]", line.c_str());
        serialRequestHandle(line);
    }
}

void taskCallbackSerialSendQueued() {
  // Serial.println("boo");
  sendSerialQueued();
}

void taskCallbackSerialSendKeepAlive() {
  serialSendKeepAlive();
}

bool serialReadLine(String& line, String lineEnd) {
    while (Serial.available() > 0)
    {
        char received = Serial.read();
        byte b = (byte)received;
        line += received;

        if (line.endsWith(lineEnd))
        {
            line = line.substring(0,line.length()-lineEnd.length());
            return true;
        }
    }
    return false;
}

// =============================================================================
// ====== Low Level General Utility Functions ==================================
// =============================================================================

String urldecode(String str) {
    String encodedString="";
    char c;
    char code0;
    char code1;
    for (int i =0; i < str.length(); i++) {
        c=str.charAt(i);
        if (c == '+') {
            encodedString += ' ';
        } else if (c == '%') {
            code0=str.charAt(++i);
            code1=str.charAt(++i);
            c = (h2int(code0) << 4) | h2int(code1);
            encodedString += c;
        } else {
            encodedString += c;
        }
        yield();
    }
    return encodedString;
}

String urlencode(String str)
{
    String encodedString="";
    char c;
    char code0;
    char code1;
    char code2;
    for (int i =0; i < str.length(); i++) {
        c=str.charAt(i);
        if (c == ' ') {
            encodedString+= '+';
        } else if (isalnum(c)) {
            encodedString+=c;
        } else {
            code1 = (c & 0xf)+'0';
            if ((c & 0xf) >9) {
                code1 = (c & 0xf) - 10 + 'A';
            }
            c = (c>>4)&0xf;
            code0 = c+'0';
            if (c > 9) {
                code0 = c - 10 + 'A';
            }
            code2 = '\0';
            encodedString += '%';
            encodedString += code0;
            encodedString += code1;
            //encodedString += code2;
        }
        yield();
    }
    return encodedString;
}

unsigned char h2int(char c) {
    if (c >= '0' && c <='9'){
        return((unsigned char)c - '0');
    }
    if (c >= 'a' && c <='f'){
        return((unsigned char)c - 'a' + 10);
    }
    if (c >= 'A' && c <='F'){
        return((unsigned char)c - 'A' + 10);
    }
    return(0);
}

void mergeJsonObjects(JsonObject& src, JsonObject& dest) {
   for (auto kvp : src) {
     dest[kvp.key] = kvp.value;
   }
}
// just using (uint32_t)String(2786573728).toInt() will result in 2147483647
uint32_t stringToUint32t(String& val) {
  if (val.length() <= 9) {
    return (uint32_t)val.toInt();
  } else {
    String notTopDigit = val.substring(1);
    uint32_t result = (uint32_t)notTopDigit.toInt();
    result = result + (1000000000 * (uint32_t)(val.substring(0,val.length()-9).toInt()));
    return result;
  }
}

String jsonCombineToString(String rootStr, String key, String rhsObjStr) {
  rootStr.replace("\"@@@INSERTHERE@@@\"", rhsObjStr);
  return rootStr;
}

String jsonCombineToString(JsonObject& root, String key, String rhsObjStr) {
  root[key] = "@@@INSERTHERE@@@";
  String rootStr;
  root.printTo(rootStr);
  return jsonCombineToString(rootStr, key, rhsObjStr);
}

String jsonCombineToString(JsonObject& root, String key, JsonObject& rhsObj) {
  String rhsObjStr;
  rhsObj.printTo(rhsObjStr);
  return jsonCombineToString(root, key, rhsObjStr);
}
