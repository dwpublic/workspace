import paho.mqtt.client as mqtt
import time
import sys
import json
import serial
import urllib

################################################################################
# The MQTT side of the bridge - probably localhost?
CONST_MQTT_HOST = "localhost"
CONST_MQTT_CONNECTION_ID = "Pi1"
CONST_MQTT_DEVICE_ID = "InfoThangBridge"

################################################################################
# The Mesh side of the bridge - probably via USB Serial
CONST_SERIAL_PORT = '/dev/ttyUSB1'
CONST_SERIAL_BAUD = 115200
CONST_SERIAL_PARITY = serial.PARITY_NONE
CONST_SERIAL_STOP_BITS = serial.STOPBITS_ONE
CONST_SERIAL_BYTESIZE = serial.EIGHTBITS

################################################################################
# general configuraiton constants
CONST_MAIN_LOOP_SLEEP = 0.05
CONST_MESH_PREFIX_COMMENT = "#"
CONST_MESH_PREFIX_MSG = "MSG:"
CONST_MESH_PREFIX_KA = "KA:"

################################################################################
# Helper functions for logging
def logInfo(msg):
    print("INFO :" + msg)

def logDebug(msg):
    print("DEBUG:" + msg)

################################################################################
# Class to represent generic message which should cover both sides of the bridge
# The message knows how to encode and decode itself in JSON, and can output
# itself as a string (for debug logging)
class Msg:
    style = ""
    source = ""
    topic = ""
    message = ""

    # Normally you would us the MsgFactory to create a Msg rather than calling
    # this directly
    def __init__(self, style, source, topic, message):
        logDebug("Msg.__init__")
        self.style = style
        self.source = source
        self.topic = topic
        self.message = message

    def toJson(self):
        logDebug("Msg.toJson")
        jsonObj = {
            "stl": self.style,
            "src": self.source,
            "tpc": self.topic,
            "msg": self.message
        }
        return json.dumps(jsonObj);

    def fromJson(self, jsonData):
        logDebug("Msg.fromJson ["+jsonData+"]")
        jsonObj = json.loads(jsonData)
        if ('stl' in jsonObj):
            self.style = jsonObj['stl']
        else:
            self.style = 'json'
        self.source = jsonObj['src']
        self.topic = jsonObj['tpc']
        self.message = urllib.unquote(jsonObj['msg']).decode('utf8')

    def toString(self):
        str = "Message Details:\r\n"
        str += "  style   =" + self.style + "\r\n"
        str += "  source  =" + self.source + "\r\n"
        str += "  topic   =" + self.topic + "\r\n"
        str += "  message =" + self.message + "\r\n"
        return str

################################################################################
# Class used to create a MSG based on the passed parameters
class MsgFactory:
    def createMsgFromMQTT(self, source, mqttMessage):
        logDebug("MsgFactory.createMsgFromMQTT")
        msg = Msg("mqtt", source, mqttMessage.topic, mqttMessage.payload.decode("utf-8"));
        return msg;

    def createMsgFromJSON(self, jsonData):
        logDebug("MsgFactory.createMsgFromJSON")
        msg = Msg("json", "", "", "");
        msg.fromJson(jsonData);
        return msg;

################################################################################
# Simple wrapper class for talking to a serial connection (usually via USB)
class SerialLink:
    connection = None
    currentLine = ""
    lastLine = ""
    endOfLine = "\r\n"

    def __init__(self, connection):
        logDebug("SerialLink.SerialLink")
        self.connection = connection
        self.lenEndOfLine = len(self.endOfLine)

    def sendLine(self, line):
        logDebug("SerialLink.sendLine")
        self.connection.write(line + self.endOfLine)

    def readLine(self):
        #logDebug("SerialLink.readLine") # this gets called a lot so uncomment only in emergency
        while (self.connection.in_waiting):
            self.currentLine += self.connection.read(1)
            if (self.currentLine.endswith(self.endOfLine)):
                self.lastLine = self.currentLine[:-1*self.lenEndOfLine]
                self.currentLine = ""
                return True
        return False

    def popLastLine(self):
        result = self.lastLine
        self.lastLine = ""
        return result

################################################################################
# Class for implementing the MQTT side of the bridge
class MQTTSide:
    client = None
    onReceiveCallback = None
    msgFactory = None

    def __init__(self):
        logDebug("MQTTSide.__init__")
        self.msgFactory = MsgFactory()

    def initialise(self, onReceiveCallback):
        logDebug("MQTTSide.initialise")
        self.onReceiveCallback = onReceiveCallback

        logDebug(" Creating New MQTT Client Instance")
        self.client = mqtt.Client(CONST_MQTT_CONNECTION_ID)
        self.client.on_message = self.onReceiveMqttMessageCallback

        logDebug(" Connecting MQTT Client Broker")
        self.client.connect(CONST_MQTT_HOST)

        logDebug(" Starting MQTT Client loop")
        self.client.loop_start()

        logDebug(" Subscribing to topic #")
        self.client.subscribe("#")

    def shutdown(self):
        logDebug("MQTTSide.shutdown")
        logDebug(" Stopping the main MQTT loop...")
        self.client.loop_stop() #stop the loop

    def send(self, message):
        logDebug("MQTTSide.send")
        self.client.publish(message.topic, message.message)

    def pollFromLoop(self):
        # Nothing to see here (yet?)
        return True

    def onReceiveMqttMessageCallback(self, client, userdata, mqttMessage):
        logDebug("MQTTSide.onReceiveMqttMessageCallback")
        logDebug(" MQTT message received...")
        msg = self.msgFactory.createMsgFromMQTT('mqtt', mqttMessage)
        logDebug(msg.toString())
        self.onReceiveCallback(msg)

################################################################################
# Class for managing the Mesh side of the bridge
class MeshSide:
    serialLink = None
    onReceiveCallback = None
    msgFactory = None

    def __init__(self):
        logDebug("MeshSide.__init__")
        self.msgFactory = MsgFactory()

    def initialise(self, onReceiveCallback):
        logDebug("MeshSide.initialise")
        self.onReceiveCallback = onReceiveCallback

        logInfo("Creating Serial Link")
        connection = serial.Serial(
            port = CONST_SERIAL_PORT,
            baudrate = CONST_SERIAL_BAUD,
            parity = CONST_SERIAL_PARITY,
            stopbits = CONST_SERIAL_STOP_BITS,
            bytesize = CONST_SERIAL_BYTESIZE
        )
        self.serialLink = SerialLink(connection)

    def shutdown(self):
        logDebug("MeshSide.shutdown")
        # TODO - implement this

    def send(self, message):
        logDebug("MeshSide.send")
        jsonString = message.toJson()
        self.serialLink.sendLine("MSG:"+jsonString)

    def pollFromLoop(self):
        #logDebug("MeshSide.pollFromLoop") # this gets called a lot so uncomment only in emergency
        if (self.serialLink.readLine()):
            line = self.serialLink.popLastLine()
            self.onReceiveMeshMessage(line)

    def onReceiveMeshMessage(self, line):
        logDebug("MeshSide.onReceiveMeshMessage")
        if (line.startswith(CONST_MESH_PREFIX_COMMENT)):
            logDebug(" Mesh log line received - ignoring:["+line+"]")
        elif (line.startswith(CONST_MESH_PREFIX_KA)):
            logDebug(" keep alive noted")
        elif (line.startswith(CONST_MESH_PREFIX_MSG)):
            logDebug(" Mesh message received...")
            msgJSON = line[len(CONST_MESH_PREFIX_MSG):]
            msg = self.msgFactory.createMsgFromJSON(msgJSON)
            logDebug("   parsed msg...")
            logDebug(msg.toString())
            self.onReceiveCallback(msg)
        else:
            logDebug(" ERROR: don't know how to handle this line:["+line+"]")

################################################################################
# Class that represents the bridge itself
class BridgeManager:
    keepMainLoopRunning = True
    sideMQTT = None
    sideMesh = None

    def __init__(self, sideMQTT, sideMesh):
        self.sideMQTT = sideMQTT
        self.sideMesh = sideMesh

    def initialise(self):
        logDebug("BridgeManager.initialise")
        self.sideMQTT.initialise(self.onReceiveMessageFromMQTT)
        self.sideMesh.initialise(self.onReceiveMessageFromMesh)

    def shutdown(self):
        logDebug("BridgeManager.shutdown")
        self.sideMQTT.shutdown()
        self.sideMesh.shutdown()
        logDebug(" Preparing to fall out of the main loop...")
        self.keepMainLoopRunning = False

    def mainLoop(self):
        while self.keepMainLoopRunning:
            self.sideMQTT.pollFromLoop()
            self.sideMesh.pollFromLoop()
            time.sleep(CONST_MAIN_LOOP_SLEEP) # wait

    def sendMessageToMQTT(self, message):
        logDebug("BridgeManager.sendMessageToMQTT")
        self.sideMQTT.send(message)

    def onReceiveMessageFromMQTT(self, message):
        logDebug("BridgeManager.onReceiveMessageFromMQTT")
        self.sendMessageToMesh(message)

    def sendMessageToMesh(self, message):
        logDebug("BridgeManager.sendMessageToMesh")
        self.sideMesh.send(message)

    def onReceiveMessageFromMesh(self, message):
        logDebug("BridgeManager.onReceiveMessageFromMesh")
        self.sideMQTT.send(message);


logDebug("App Starting")
bridge = None

logDebug("App Constructing stuff")
mqttSideOfBridge = MQTTSide()
meshSideOfBridge = MeshSide()
bridge = BridgeManager(mqttSideOfBridge, meshSideOfBridge)

logDebug("App Initialising stuff")
bridge.initialise()

logDebug("App Starting bridges main loop")
bridge.mainLoop()

logDebug("Exiting main loop")
