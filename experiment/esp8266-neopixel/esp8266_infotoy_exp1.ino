
/*
 * InfoToy Project - Experiment 1
 * 
 * InfoToy:
 *   Small portable or usb powered device with the appearance of a toy or art, that
 *   can convey information received via mqtt via wifi
 *          
 * Goal:    
 *   A Sketch that allows...
 *     * an esp8266
 *     * to connect to a wifi network
 *     * subscribe to mqtt messages
 *     * control an RGB neopixel based on received mqtt messages
 * 
 * INO File Prefix Guide:
 *   a* = config
 *   l* = utility code for interacting with local hardware
 *   o* = utility code for interacting with off-board (external) systems
 *   
 * 
 */

// ino files prefix as follows



