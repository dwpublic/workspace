
void setup() {
  Serial.begin(115200);
  Serial.println("MainSetup - start");

  BlinkerSetup(BUILTIN_LED);
  BlinkerSetPeriod(100);

  Serial.println("MainSetup - wait for serial watch to see what I'm doing ;)");
  Serial.println("");
  int i = 0;
  for (i = 0; i < 30; i++) {
    Serial.print(".");
    delay(100);
  }
  Serial.println("");
  Serial.println("MainSetup - right - let's get on with the setup then...");

  Serial.println("MainSetup - load config");
  Config.spiffs_begin();
  Config.load();
  Config.log();

  Serial.println("MainSetup - increment reboot count");
  Config.reboots = Config.reboots + 1;
//  Config.reboots = 0;
  Config.save();

  Serial.println("MainSetup - initialise wifi");
  wifi_setup(Config.wifiExternalSSID, Config.wifiExternalPwd);

  Serial.println("MainSetup - initialise mqtt");
  MQTT.setExternalCallback(mqtt_pubSubCallback);
  MQTT.setup(espClient, Config.mqttServer, Config.mqttId);

  Serial.println("MainSetup - initialise neopixel(s)");
  NeoPixels.setup();

  Serial.println("MainSetup - wait for 5 seconds");
  delay(5000);

  Serial.println("MainSetup - end");
}
