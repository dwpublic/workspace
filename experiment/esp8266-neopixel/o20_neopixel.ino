#include <Adafruit_NeoPixel.h>

#define NEO_PIXEL_PIN 14
#define NEO_PIXEL_COUNT 1

// Parameter 1 = number of pixels in strip
// Parameter 2 = Arduino pin number (most are valid)
// Parameter 3 = pixel type flags, add together as needed:
//   NEO_KHZ800  800 KHz bitstream (most NeoPixel products w/WS2812 LEDs)
//   NEO_KHZ400  400 KHz (classic 'v1' (not v2) FLORA pixels, WS2811 drivers)
//   NEO_GRB     Pixels are wired for GRB bitstream (most NeoPixel products)
//   NEO_RGB     Pixels are wired for RGB bitstream (v1 FLORA pixels, not v2)
//   NEO_RGBW    Pixels are wired for RGBW bitstream (NeoPixel RGBW products)

Adafruit_NeoPixel neoPixels = Adafruit_NeoPixel(NEO_PIXEL_COUNT, NEO_PIXEL_PIN, NEO_GRB + NEO_KHZ800);
//Adafruit_NeoPixel neoPixels = Adafruit_NeoPixel(NEO_PIXEL_COUNT, NEO_PIXEL_PIN, NEO_RGB + NEO_KHZ800);

class NeoPixelsClass {
  private:

  public:

    void setup() {
      Serial.println("NeoPixels setup - start");

      neoPixels.begin();

      Serial.println("NeoPixels setup - end");
    }

    void setPixelColor(uint8_t pixel, uint8_t red, uint8_t green, uint8_t blue) {
      Serial.println("NeoPixels setPixelColour[rgb] - start");

      neoPixels.setPixelColor(0, green, red, blue);
      neoPixels.show();

      Serial.println("NeoPixels setPixelColour[rgb] - end");
    }

    void setPixelColor(uint8_t pixel, uint16_t color) {
      Serial.println("NeoPixels setPixelColour[i] - start");

      neoPixels.setPixelColor(0, color);
      neoPixels.show();

      Serial.println("NeoPixels setPixelColour[i] - end");
    }

    void setPixelColor(uint8_t pixel, String color) {
      Serial.println("NeoPixels setPixelColour[str] - start");

      Serial.println("NeoPixels setPixelColour[str] - setting color from string ["+color+"]");
      if (color.startsWith("#")) {
          String colpart = color.substring(1,3);
          int r = strtol(colpart.c_str(), NULL, 16);
          Serial.println("NeoPixels setPixelColour[str] - parsing colour r ["+colpart+"] = ["+String(r)+"]");
          colpart = color.substring(3,5);
          int g = strtol(colpart.c_str(), NULL, 16);
          Serial.println("NeoPixels setPixelColour[str] - parsing colour g ["+colpart+"] = ["+String(g)+"]");
          colpart = color.substring(5,7);
          int b = strtol(colpart.c_str(), NULL, 16);
          Serial.println("NeoPixels setPixelColour[str] - parsing colour b ["+colpart+"] = ["+String(b)+"]");

          Serial.println("NeoPixels setPixelColour[str] - setting color as ["+String(r)+","+String(g)+","+String(b)+"]");
          return setPixelColor(pixel, r, g, b);
      }

    //   neoPixels.setPixelColor(0, color);
    //   neoPixels.show();

      Serial.println("NeoPixels setPixelColour[str] - end");
    }

};

NeoPixelsClass NeoPixels;
