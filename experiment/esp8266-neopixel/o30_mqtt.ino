#include <WString.h>
#include <PubSubClient.h>

using namespace std;

PubSubClient mqttClient(espClient);

class MqttClass {
  private:
    String _id;
    long lastMsg = 0;
    char msg[50];
    int value = 0;

  public:

    void setup(WiFiClient wifiClient, String mqttServer, String id) {
      Serial.println("MQTT setup - start");
      _id = id;
    //   Serial.println("MQTT setup - setting wifi client");
    //   mqttClient.setClient(wifiClient);
      Serial.println(("MQTT setup - mqtt server set to [" + mqttServer + "]").c_str());
      mqttClient.setServer(mqttServer.c_str(), 1883);
      String msg = "MQTT Setup - id set to [" + _id + "]";
    //   msg += ;
      Serial.println(msg.c_str());
      reconnect();
      Serial.println("MQTT setup - end");
    }

    void setExternalCallback(MQTT_CALLBACK_SIGNATURE) {
      Serial.println("MQTT setExternalCallback - start");
      mqttClient.setCallback(callback);
      Serial.println("MQTT setExternalCallback - end");
    }

    void handleSubscribedMessage(char* topic, byte* payload, unsigned int length) {
      Serial.println("MQTT handleSubscribedMessage - start");
      String sTopic = topic;
      serialIncommingMsg(sTopic, payload, length);

      vector<String> topicParts = strsplit(sTopic, '/');

      String sTopicTarget = topicParts[0];
      String topicLog = "MQTT handleSubscribedMessage - topic target = [" + sTopicTarget + "]";
      Serial.println(topicLog.c_str());

      if (sTopicTarget.equals(_id)) {
        Serial.println("MQTT handleSubscribedMessage - incomming message target match - this is for me");

        if (topicParts[1].equals("primary") && topicParts[2].equals("color")) {
            String color((char*)payload);
            NeoPixels.setPixelColor(0, color);
        }
        // Switch on the LED if an 1 was received as first character
        if ((char)payload[0] == '1') {
          Serial.println("MQTT handleSubscribedMessage - led on");
          builtInLedOn();
        } else {
          Serial.println("MQTT handleSubscribedMessage - led off");
          builtInLedOff();
        }
      }

      Serial.println("MQTT handleSubscribedMessage - end");
    }

    void serialIncommingMsg(String sTopic, byte* payload, unsigned int length) {
      Serial.print("MQTT serialIncommingMsg - Message arrived [");
      Serial.print(sTopic.c_str());
      Serial.print("] ");
      for (int i = 0; i < length; i++) {
        Serial.print((char)payload[i]);
      }
      Serial.println();
    }

    void publish(String topic, String msg) {
        Serial.println("MQTT publish - start");

        String fullTopic = _id + "/" + topic;
        mqttClient.publish(fullTopic.c_str(), msg.c_str());

        Serial.println("MQTT publish - end");
    }

    void reconnect() {
      Serial.println("MQTT reconnect - start");
      // Loop until we're reconnected
      while (!mqttClient.connected()) {
        Serial.print("MQTT reconnect - Attempting MQTT connection... ");
        // Attempt to connect
        if (mqttClient.connect("ESP8266Client")) {
          Serial.println("MQTT reconnect - connected");
          // Once connected, publish an announcement...
          publish("log", "connected");
          // ... and resubscribe
          String subscribeTopic = _id + "/#";
          mqttClient.subscribe(subscribeTopic.c_str());
        } else {
          Serial.print("failed, rc=");
          Serial.print(mqttClient.state());
          Serial.println(" try again in 5 seconds");
          // Wait 5 seconds before retrying
          delay(5000);
        }
      }
      Serial.println("MQTT reconnect - end");
    }

    void loop() {
    //   Serial.println("MQTT loop - start");

      if ( ! mqttClient.connected()) {
        Serial.println("MQTT loop - reconnecting");
        reconnect();
      }
    //   Serial.println("MQTT loop - hitting client loop");
      mqttClient.loop();

      long now = millis();
      if (now - lastMsg > 2000) {
        Serial.println("MQTT loop - pulse");
        lastMsg = now;
        ++value;
        // snprintf (msg, 75, "hello world #%ld", value);
        // Serial.print("MQTT loop - Publish message: ");
        // Serial.println(aliveMsg.c_str());
        String message = "hello world [" + String(value) + "]";
        publish("log.alive", message);
      }
      delay(10);

    //   Serial.println("MQTT loop - end");
    }
};

MqttClass MQTT;

void mqtt_pubSubCallback(char* topic, byte* payload, unsigned int length) {
  MQTT.handleSubscribedMessage(topic, payload, length);
}
