
bool loop_first_run = true;

void loop() {
  // Serial.println("MainLoop - start");
  // NeoPixels.setPixelColor(0,0,0,200);
  // delay(100);
  // NeoPixels.setPixelColor(0,0,0,0);
  // delay(100);

  if (loop_first_run) {
    Serial.println("MainLoop - first run");
    loop_first_run = false;
    // First run in the loop code here
    BlinkerSetPeriod(200);
  }

  MQTT.loop();

  // Serial.println("MainLoop - end");
}
