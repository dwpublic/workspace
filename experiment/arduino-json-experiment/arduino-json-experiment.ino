#include <ArduinoJson.h>

void typeof(String a){ Serial.println("it a String a");}
void typeof(int a)   { Serial.println("it a int a");}
void typeof(char* a) { Serial.println("it a char* a");}
void typeof(JsonObject& a) { Serial.println("it a JsonObject& a");}
void typeof(JsonArray& a) { Serial.println("it a JsonArray& a");}


void insertString(JsonObject& root, String key, String value) {
  root[key] = value;
}

JsonObject& insertObject(JsonObject& root, String key) {
  return root.createNestedObject(key);
}

String combineToString(JsonObject& root, String key, JsonObject& rhsObj) {
  root[key] = "@@@INSERTHERE@@@";
  String rootStr;
  root.printTo(rootStr);
Serial.println("# " + rootStr);
  String rhsObjStr;
  rhsObj.printTo(rhsObjStr);
Serial.println("# " + rhsObjStr);
  rootStr.replace("\"@@@INSERTHERE@@@\"", rhsObjStr);
Serial.println("# " + rootStr);
  return rootStr;
}

void setup() {

  Serial.begin(115200);

  char json1[] = "{\"first\":\"hello\",\"then\":{\"inner\":\"outie\"},\"second\":\"world\"}";
  DynamicJsonBuffer jsonBuffer1;
  JsonObject& root1 = jsonBuffer1.parseObject(json1);
  
  char json2[] = "{\"horse\":\"four\",\"when\":{\"after\":\"the other\"},\"Dolphins\":\"one\"}";
  DynamicJsonBuffer jsonBuffer2;
  JsonObject& root2 = jsonBuffer2.parseObject(json2);

  String json12 = combineToString(root1, "muppets", root2);
  
  DynamicJsonBuffer jsonBuffer;
  JsonObject& root = jsonBuffer.parseObject(json12);

  printIt(root);
}

void printIt(JsonObject& root, String path) {
  // using C++11 syntax (preferred):
  for (auto kv : root) {
      if (kv.value.is<char*>()) {
        Serial.printf("%s.%s -> %s (string)", path.c_str(), kv.key, kv.value.as<char*>());
        Serial.println("");
      } else if (kv.value.is<JsonObject&>()) {
        Serial.printf("%s.%s -> ... (JsonObject)", path.c_str(), kv.key);
        Serial.println("");
        JsonObject& subElement = kv.value.as<JsonObject&>();
        String subPath = path + "." + String(kv.key);
        printIt(subElement, subPath);
      }
  }
}

void printIt(JsonObject& root) {
  printIt(root, "");
}


void loop() {
  // put your main code here, to run repeatedly:

}
