/*
 Adapted from ESP8266 Blink by Simon Peter

 For NodeMCU ESP-12E
 
 Blink the blue LED on the ESP-01 module if a button is making D2 high

 Wire a 'pull-down' resistor between D2 and GND
 Connect / Disconnect 3v3 to D2 to force it up

 When connected the onboard blue LED will blink on 1 sec, off 1 sec
 When not connected the onboard blue LED will remain off.
 
*/

const int buttonPin = 5;     // the number of the pushbutton pin
int blinkPeriod = 1000;

void setup() {
  Serial.begin(115200);
  pinMode(LED_BUILTIN, OUTPUT);     // Initialize the LED_BUILTIN pin as an output
  pinMode(buttonPin, INPUT);

  int buttonState = digitalRead(buttonPin);
  if (buttonState == 1) {
    blinkPeriod = 250;
  }
}

// the loop function runs over and over again forever
void loop() {
  int buttonState = digitalRead(buttonPin);
  if (buttonState == 1) {
    Serial.println("Button On");
    digitalWrite(LED_BUILTIN, LOW);  // Turn the LED on (Note that LOW is the voltage level
                                     // but actually the LED is on; this is because  
                                     // it is active low on the ESP-01)
    delay(blinkPeriod);                     // Wait for a second
    digitalWrite(LED_BUILTIN, HIGH);  // Turn the LED off by making the voltage HIGH
  } else {
    Serial.println("Button Off");
  }
  delay(blinkPeriod);                      // Wait for one seconds (to demonstrate the active low LED)
}
