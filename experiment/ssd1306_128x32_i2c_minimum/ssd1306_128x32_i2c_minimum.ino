#include <SPI.h>
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>

#define OLED_RESET 16
Adafruit_SSD1306 display(OLED_RESET);

int count = 0;

void setup()   {                
  Serial.begin(115200);

  // by default, we'll generate the high voltage from the 3.3v line internally! (neat!)
  display.begin(SSD1306_SWITCHCAPVCC, 0x3C);  // initialize with the I2C addr 0x3C (for the 128x32)
  // init done
  
  // text display tests
  display.clearDisplay();
  display.setTextSize(2);
  display.setTextColor(WHITE);
  display.setCursor(0,0);

  display.println("\r\nStart X");
  display.display();

  delay(3000);
}

void loop() {
  display.clearDisplay();
  display.setCursor(0,0);
  String txt = "\r\nCount: " + String(count);
  Serial.println(txt.c_str());
  display.println(txt);
  display.display();
  
  delay(1000);
  count++;
}


