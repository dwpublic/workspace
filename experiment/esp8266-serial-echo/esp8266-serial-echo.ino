/*
 * example serialReadLine() function for reading Serial.read()
 * strings a line at a time without blocking
 *
 * Author: Dave Amphlett
 *
 */

void setup() {
    Serial.begin(115200);    // opens serial port, sets data rate to 9600 bps
}

bool serialReadLine(String& line, char* lineEnd) {
    while (Serial.available() > 0)
    {
        char received = Serial.read();
        byte b = (byte)received;
        line += received;

        if (line.endsWith(lineEnd))
        {
            line = line.substring(0,line.length()-strlen(lineEnd));
            return true;
        }

    }

    return false;
}

String line;
unsigned long lastAliveMsgTime = 0;
unsigned long lastAliveMsgNum = 0;
unsigned long aliveMsgEvery = 3000;

void loop() {
  if (serialReadLine(line, "\r")) {
    Serial.print("#Loop got the line:["); Serial.print(line); Serial.println("]");
    if (line == "boo") {
      Serial.println("=That's scary!");
    } else if (line == "help") {
      Serial.println("=not much I can do from here I'm afraid");
    }
    line = "";
  }
  unsigned long now = millis();
  unsigned long timeSinceLastAlive = (now - lastAliveMsgTime);
  if (timeSinceLastAlive > aliveMsgEvery) {
    Serial.print("#Still alive ["); Serial.print(lastAliveMsgNum); Serial.print("] ["); Serial.print(timeSinceLastAlive); Serial.println("]");
    lastAliveMsgTime = now;
    lastAliveMsgNum++;
  }
  delay(100);
}
