#include <SPI.h>
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>
#include <TaskScheduler.h>

#define OLED_RESET 16
Adafruit_SSD1306 display(OLED_RESET);

void taskCallbackShowCount();

Scheduler _userScheduler; // to control your personal task
//Task taskObjShowCount( TASK_SECOND * 1 , TASK_FOREVER, &taskCallbackShowCount );
Task taskObjShowCount( TASK_MILLISECOND * 100 , TASK_FOREVER, &taskCallbackShowCount );

int count = 0;

void setup()   {                
  Serial.begin(115200);

  // by default, we'll generate the high voltage from the 3.3v line internally! (neat!)
  display.begin(SSD1306_SWITCHCAPVCC, 0x3C);  // initialize with the I2C addr 0x3C (for the 128x32)
  // init done
  
  // text display tests
  display.clearDisplay();
  display.setTextSize(2);
  display.setTextColor(WHITE);
  display.setCursor(0,0);

  display.println("\r\nStart X");
  display.display();

  _userScheduler.init();
  _userScheduler.addTask( taskObjShowCount);
  taskObjShowCount.enable();

  delay(3000);
}

void loop() {
  _userScheduler.execute();
}


void taskCallbackShowCount() {
  display.clearDisplay();
  display.setCursor(0,0);
  String txt = "\r\nCount: " + String(count);
  Serial.print(txt.c_str());
//  display.println(txt);
  display.setTextSize(2);
  display.print("\r\nCount:");
  display.setTextSize(1);
  display.setCursor(display.getCursorX(), display.getCursorY()+5);
  display.println(count);
  display.display();
  count++;
}



