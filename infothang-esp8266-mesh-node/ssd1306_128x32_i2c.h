/*********************************************************************
This is an example for our Monochrome OLEDs based on SSD1306 drivers

  Pick one up today in the adafruit shop!
  ------> http://www.adafruit.com/category/63_98

This example is for a 128x32 size display using I2C to communicate
3 pins are required to interface (2 I2C and one reset)

Adafruit invests time and resources providing this open source code,
please support Adafruit and open-source hardware by purchasing
products from Adafruit!

Written by Limor Fried/Ladyada  for Adafruit Industries.
BSD license, check license.txt for more information
All text above, and the splash screen must be included in any redistribution
*********************************************************************/

#include <SPI.h>
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>

#define OLED_RESET 15 //4
Adafruit_SSD1306 display(OLED_RESET);

#if (SSD1306_LCDHEIGHT != 32)
#error("Height incorrect, please fix Adafruit_SSD1306.h!");
#endif

void ssd1306_clear();
void ssd1306_showMsg(String line1, String line2, String line3, int size);
void ssd1306_showMsg(String line1, String line2, int size);
void ssd1306_showMsg(String line1, int size);


void ssd1306_setup()   {
  // by default, we'll generate the high voltage from the 3.3v line internally! (neat!)
  Serial.printf("#Setting up the SSD1306 i2c oled display\r\n");
  display.begin(SSD1306_SWITCHCAPVCC, SSD1306_I2C_ADDRESS, false);  // initialize with the I2C addr 0x3C (for the 128x32)
  delay(1);

  Serial.printf("#Clearing the oled display\r\n");
  display.clearDisplay();
  delay(1);
}

void ssd1306_loop() {

}

void ssd1306_clear() {
  display.clearDisplay();
  display.display();
}

void ssd1306_showMsg(String line1, String line2, String line3, int size) {
  display.clearDisplay();
  display.setTextColor(WHITE);
  display.setCursor(0,0);
  display.setTextSize(size);
  Serial.printf("#writing message to oled display line1:%s\r\n", line1.c_str());
  display.println(line1);
  Serial.printf("#writing message to oled display line2:%s\r\n", line2.c_str());
  display.println(line2);
  Serial.printf("#writing message to oled display line3:%s\r\n", line3.c_str());
  display.println(line3);
  Serial.printf("#Updating the display\r\n");
  display.display();
}

void ssd1306_showMsg(String line1, String line2, int size) {
  ssd1306_showMsg(line1, line2, "", size);
}

void ssd1306_showMsg(String line1, int size) {
  ssd1306_showMsg(line1, "", "", size);
}
