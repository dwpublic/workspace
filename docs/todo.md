# InfoThings ToDo

1. **MQTT<->PainlessMesh Bridge on Pi & connected ESP8266**
1. Write Arduino Library to emulate MQTT over painlessMesh
    1. Python script to bridge RPi MQTT msgs to ESP8266 Serial painlessMesh msg
        * Local Program: [local python project](../rpi-esp8266-mqtt-bridge/rpi-python/)
        * Make python script a service on RPi
          * https://www.dexterindustries.com/howto/run-a-program-on-your-raspberry-pi-at-startup/#systemd
    1. ESP8266 code to bridge painlessMesh msgs to Serial RPi MQTT msgs
        * Local Program: [local python project](../rpi-esp8266-mqtt-bridge/esp8266-rpi-mesh-bridge/)
1. Write Arduino Library for nodes on the painlessMesh to respond to bridged messages
    1. Python script to bridge RPi MQTT msgs to ESP8266 Serial painlessMesh msg
        * Local Program: [local python project](../rpi-esp8266-mqtt-bridge/rpi-python/)
1. Get Mosquitto running on Pi
    1. Secure MQTT
        * check this: https://www.digitalocean.com/community/tutorials/how-to-install-and-secure-the-mosquitto-mqtt-messaging-broker-on-ubuntu-16-04

# InfoThings Done

* ~~**ESP8266 dev envrionment Setup**~~
  * ~~Arduino IDE talking to ESP8266~~
      * Local Doc: [pdf](arduino-ide-for-esp8266/Using-Arduino-IDE_Adafruit-HUZZAH-ESP8266-breakout_Adafruit-Learning-System.pdf)
  * ~~Get PainlessMesh working on ESP8266~~
      * Local Lib: [zip](../libs/painlessMesh-master-eb17cb742ac0f7a5db123ddcaa3db233fd7b3c94.zip)
      * Git Repo: https://gitlab.com/painlessMesh/painlessMesh

* ~~**Raspberry Pi Setup**~~
  * ~~Get Raspbian Stretch Lite on Pi Zero~~
      * Etcher for creating sd cards: https://etcher.io/
      * Raspbian download: https://www.raspberrypi.org/downloads/raspbian/
  * ~~Install Python dependencies on Pi~~
    ```bash
    sudo apt-get install python-pip
    pip install pyserial
    pip install paho-mqtt
    ```

* **MQTT<->PainlessMesh Bridge on Pi & connected ESP8266**
  * ~~Get Pi talking serial to ESP8266~~
        * Local Doc: [pdf](rpi-to-esp8266-serial/Connect-an-ESP8266-to-Your-RaspberryPi_3-Steps.pdf)

      1. Put this on an ESP8266 [local arduino proj](../esp8266-serial-echo/)
      1. Put this on a Pi - [local python proj](../rpi-serial-monitor/)
      1. Connect the Pi spare usb to the ESP8266 USB
      1. Run this on the Pi
      ```bash
      python serialtest.py
      ```
  * ~~Get Mosquitto running on Pi~~
      * ~~Install MQTT server and clients (mosquitto) on Pi~~
        ```bash
        sudo apt install mosquitto mosquitto-clients
        ```
  * ~~Design and implement Bridge Protocol~~
      * [local markdown doc](mqtt-mesh-bridge-protocol.md)
