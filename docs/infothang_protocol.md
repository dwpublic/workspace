# infothang Protocol

## General Communication Format

All infothang messages are json formatted and have the following top-level format:

```
{
  'direction': '[in|out]',
  'channel' : '{channelName}',
  'info' : '{the information}',
  'type' : 'change'
}
```

| *field* | *direction* | *details* |
|-----|-----|-----|
| direction | in,out | direction of the communication from the infothang's perspective <br>in=input, out=output|
| channel | in,out | each infothang has certain named in and out channels<br>although there are a defacto standard list that infothangs should attempt to support if possible |
| info | in | the information received by the infothang on the specified channel |
| info | out | the information to be output by the infothang on the specified channel |
| type | change | for direction:in the information is being published because it has changed |
| type | state | for direction:in the information is being published because a status request was received |

### Quick Examples

Message sent to an infothang to 'turn-on' it's primary output channel

```
{
  'direction': 'out',
  'channel': 'primary',
  'info': 1
}
```

Message sent from an infothang when it's primary input channel is activated

```
{
  'direction': 'in',
  'channel': 'primary',
  'info': 1,
  'type': 'change'
}
```

## Outputting information
