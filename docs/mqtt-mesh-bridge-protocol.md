# MQTT to PainlessMesh Protocol

## Raspberry Pi / ESP8266 Serial Link

1. General Communication Format

All serial link communications have the following top-level format:

```
[header][body]\r\n
```

  1. Valid Headers
      1. `#`  - Comment, please ignore.
          * The `[body]` in this case will simply be a text string
          1. `KA:` - A Keep-Alive message between sides of the bridge.
            * The `[body]` in this case will an integer, generally increasing although it will loop back around occasionally
            * Example:
              KA:28337
          1. `CTL:` - A Control Instruction between sides of the bridge.
              * The `[body]` in this case will be a JSON encoded message
              * Example:
                CTL:{'command':'status'}
          1. `RSP:` - A response to a sent message.
              * The `[body]` in this case will be a JSON encoded message
          1. `MSG:` - A trans-bridge message.
              * A message from one side of the bridge, for transmission on the otherside of the bridge
              * The `[body]` in this case will be a JSON encoded message
              * Example:
                MSG:{"at":"20180601171324","src":"bridge-123456789","tpc":"painlessMesh/to/2786573728":"msg":{"out":"light1","val":"#ff0000"}}
                MSG:{"src":"mqtt","tpc":"painlessMesh/to/2786573728","msg":"boo"}
                MSG:{"src":"mqtt","tpc":"painlessMesh/to/gateway","msg":"getStatus"}

  1. MSG Body - JSON encoded message format Fields:
      * at
          - YYMMDDhhmmss of when the message was generated - useful for logging / debugging
      * src
          - from mesh-side this is the id of the node source of the message
      * tpc
          - this maps directly to the MQTT topic
      * msg
          - this maps directly to the MQTT message, which may or may not be JSON encoded

### Rpi to ESP8266 Message
